//Idetify the components that we will use to build the page
import Banner from './../components/Banner';
import Footer from './../components/Footer';

const data = {
	title: '404 Page Not Found',
	content: 'The page you are looking for does not exist'
}

export default function ErrorPage (){
	return (
		<>
		<Banner bannerData={data} />
		<Footer />
		</>
		);
}