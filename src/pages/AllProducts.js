import {Container} from 'react-bootstrap'
import Hero from './../components/Banner';
import ProductCard from './../components/AdminProductCard';
import UserContext from '../UserContext'
import { useState, useEffect, useContext } from 'react';
import Footer from './../components/Footer';
const bannerDetails = {
	title: 'Products',
	content: 'Featured Products Base on Your Search'
}
export default function Products () {
	const { user } = useContext(UserContext);
	const [productsCollection, setProductCollection] = useState([]);
	 let token = localStorage.getItem('access');
	 
	useEffect(() => {
		fetch('https://whispering-gorge-43888.herokuapp.com/products/', {
			headers:{
				Authorization: `Bearer ${token}`
			}
		}).then(res => res.json()).then(convertedData => {
			/*console.log(convertedData);*/
			setProductCollection(convertedData.map(product => {
				return(
					<ProductCard key={product._id} productProp={product}/>
					)
			}))
		})
	}, [user, token])
	return (
		<>
			<Hero bannerData={bannerDetails}/>
			<Container className="text-center">
				{productsCollection}
			</Container>
			<Footer />
		</>
		);
}